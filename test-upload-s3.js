if (Meteor.isClient) {

  /*****************************************************************************/
  /* cropImageModal Template
   /*****************************************************************************/
  Template.cropImageModal.onRendered(function(){
    var $image,
        cropBoxData,
        canvasData;
    Session.set('updateCropImage','init');

    //$('#cropper-modal').on('shown.bs.modal', function () {
    //  $('#loadingCropImage').show();
    //  $('#cropper-section').hide();
    //});

    $('#cropper-modal').on('hidden.bs.modal', function () {
      $image = $('#cropper-section > img');
      cropBoxData = $image.cropper('getCropBoxData');
      canvasData = $image.cropper('getCanvasData');
      $('#closeCropImageModal').removeClass('disabled');
      $image.cropper('destroy');
    });
  });

  Template.cropImageModal.events({
    'click #onUploadPhoto': function (event, tpl) {
      Session.set('updateCropImage','updated');
      $(event.currentTarget).addClass('disabled');
      tpl.$('#closeCropImageModal').addClass('disabled');
      tpl.$('.progress').show();

      // For normal image in user Profile
      var canvas = tpl.$('#cropper-section > img').cropper('getCroppedCanvas',{
        width: 160,
        height: 160
      });
      var dataURL = canvas.toDataURL('image/png');
      var blob = dataURItoBlob(dataURL);

      var formData = new FormData();
      var key = 'haunguyen' +'/' + (new Date()).getTime() + '-normal.png';
      formData.append('key', key);
      formData.append('acl', 'public-read-write');
      formData.append('file', blob);

      // For small image in leaderboard
      var canvasSmall = tpl.$('#cropper-section > img').cropper('getCroppedCanvas',{
        width: 45,
        height: 45
      });
      var dataURLSmall = canvasSmall.toDataURL('image/png');
      var blobSmall = dataURItoBlob(dataURLSmall);
      var formDataSmall = new FormData();
      var keySmall = key.replace('normal.png', 'small.png');
      formDataSmall.append('key', keySmall);
      formDataSmall.append('acl', 'public-read-write');
      formDataSmall.append('file', blobSmall);

      ajaxUpload(formData).success(function() {
        ajaxUpload(formDataSmall);
        console.log ("http://boonapp.io.s3-ap-southeast-1.amazonaws.com/" + key);
        // Meteor.call('updateAvatar', "https://boomfantasy.s3-us-west-2.amazonaws.com/", key, function(err) {
        //   if (err) {
        //     App.track("Error while update user photo in mongo", {error: err});
        //     alert("Error:" + err);
        //     return;
        //   }
        //   else {
        //     App.track("User photo - updated successfully", {error: err});
        //   }
        // });
        $(event.currentTarget).removeClass('disabled');
        tpl.$('.progress').hide();
        tpl.$('#cropper-modal').modal('hide');

      });
    },
    'click #closeCropImageModal': function(event, tpl){
      tpl.$('#cropper-modal').modal('hide');
    }
  });

  /*****************************************************************************/
  /* FUNCTION
   /*****************************************************************************/
  var progressHandlingFunction = function (e){
    if(e.lengthComputable){
      // compute progress in %
      //Documents.update(_id, {$set: {
      //  status: {
      //    label:'Uploading ...',
      //    progress: ((e.loaded / e.total) * 100).toFixed(0)
      //  }
      //}});
      $('#upload-progress-bar').attr('aria-valuenow', ((e.loaded / e.total) * 100).toFixed(0));
      $('#upload-progress-bar').css('width', ((e.loaded / e.total) * 100).toFixed(0) + "%");
      $('#upload-progress-bar').html(((e.loaded / e.total) * 100).toFixed(0) + '%');
    }
  };

  var dataURItoBlob = function (dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
    else
      byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
  };

  var ajaxUpload = function(formData){
    return $.ajax({
      url:  "http://boonapp.io.s3-ap-southeast-1.amazonaws.com/",  //Server script to process data
      type: 'POST',
      xhr: function() {  // Custom XMLHttpRequest
        var myXhr = $.ajaxSettings.xhr();
        if(myXhr.upload){ // Check if upload property exists
          myXhr.upload.addEventListener('progress', progressHandlingFunction, false); // For handling the progress of the upload
        }
        return myXhr;
      },
      //Ajax events
      //beforeSend: beforeSendHandler,
      error: function(err){
        App.track("Error while uploading photo to S3", {error: err});
      },
      // Form data
      data: formData,
      //Options to tell jQuery not to process data or worry about content-type.
      cache: false,
      contentType: false,
      processData: false,
      crossDomain: true
    });
  };

  // counter starts at 0
  Session.setDefault('counter', 0);

  Template.hello.helpers({
    counter: function () {
      return Session.get('counter');
    }
  });

  Template.hello.events({
    'click button': function () {
      // increment the counter when button is clicked
      Session.set('counter', Session.get('counter') + 1);
    },
    'change #fileSelect': function (event, tpl) {
    event.preventDefault();

    event = event.originalEvent;
    var target = event.dataTransfer || event.target,
        file = target && target.files && target.files[0],
        options = {
          canvas: true
        };

    console.log("started");
    if (!file) {
      return false;
    }
    // Use the "JavaScript Load Image" functionality to parse the file data
    loadImage.parseMetaData(file, function(data) {

      // Get the correct orientation setting from the EXIF Data
      if (data.exif) {
        options.orientation = data.exif.get('Orientation');
      }
      // Load the image from disk and inject it into the DOM with the correct orientation
      loadImage(
          file,
          function(canvas) {
            Meteor.setTimeout(function(){
              var imgDataURL = canvas.toDataURL();
              var $image = $('#cropper-section > img');
              $image.attr('src', imgDataURL);
              Session.set('updateCropImage','updating');
              var cropBoxData = $image.cropper('getCropBoxData');
              var canvasData = $image.cropper('getCanvasData');
              $('#loadingCropImage').hide();
              $('#cropper-section').show();
              $('#onUploadPhoto').removeClass('disabled');
              $('#closeCropImageModal').removeClass('disabled');
              $image.cropper({
                autoCropArea: 0.5,
                aspectRatio: 1/1,
                built: function () {
                  // Strict mode: set crop box data first
                  console.log ('building....');
                  $image.cropper('setCropBoxData', cropBoxData);
                  $image.cropper('setCanvasData', canvasData);
                },
                crop: function(e) {
                  //console.log(e.rotate);
                }
              });
            }, 100);

          },
          options
      );
    });
    // show cropper modal for allow user crop image
    $('#loadingCropImage').show();
    $('#cropper-section').hide();
    $('#cropper-modal').modal({
      backdrop: 'static',
      keyboard: false
    });
  }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
